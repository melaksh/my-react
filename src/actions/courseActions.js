import * as Types from './actionTypes';
import courseApi from '../api/mockCourseApi';
import { beginAjaxCall, ajaxCallError } from './ajaxStatusActions';

export function createCourse(course){
  return { type: Types.CREATE_COURSE, course};
}

export function loadCourses(){
  return function(dispatch){
    dispatch(beginAjaxCall());
    return courseApi.getAllCourses().then(courses => {
      dispatch(loadCoursesSuccess(courses));
    }).catch(error =>{
      throw(error);
    });

  };
}

function loadCoursesSuccess(courses){
  return {type: Types.LOAD_COURSES_SUCCESS, courses};
}

export function saveCourse(course){
  return function(dispatch, getState){
    dispatch(beginAjaxCall());
    return courseApi.saveCourse(course).then(savedCourse => {
      course.id ? dispatch(updateCourseSuccess(savedCourse)) :
      dispatch(createCourseSuccess(savedCourse));
    }).catch(error => { 
      dispatch(ajaxCallError(error));
      throw(error);
     });
  };
}

function updateCourseSuccess(course) {
  return { type: Types.UPDATE_COURSE_SUCCESS, course};
}

function createCourseSuccess(course) {
  return { type: Types.CREATE_COURSE_SUCCESS, course};
}


